import 'package:flutter/material.dart';
import 'package:qrcode/qrcode.dart';

void main() {
  runApp(BarcodeScanner());
}

class BarcodeScanner extends StatefulWidget {
  @override
  _BarcodeScannerState createState() => _BarcodeScannerState();
}

class _BarcodeScannerState extends State<BarcodeScanner> {
  QRCaptureController _captureController = QRCaptureController();
  bool _isScannerEnabled = false;
  String _barcodeData;

  @override
  void initState() {
    super.initState();
    _captureController.onCapture((data) => _onBarcodeScanned(data));
  }

  _onBarcodeScanned(String data) {
    print(data);
    setState(() {
      _barcodeData = data;
      _isScannerEnabled = false;
    });
  }

  _onScanButtonPressed() {
    _isScannerEnabled
        ? _captureController.pause()
        : _captureController.resume();

    setState(() {
      _isScannerEnabled = !_isScannerEnabled;
      _barcodeData = null;
    });
  }

  Widget _buildCenterContainer() {
    return Center(
      child: Text(
        _barcodeData != null
            ? 'Barcode data: $_barcodeData'
            : 'Press Scan to start scanning',
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          body: Stack(
        children: <Widget>[
          _isScannerEnabled
              ? QRCaptureView(
                  controller: _captureController,
                )
              : _buildCenterContainer(),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              color: Colors.white,
              height: 90,
              child: Center(
                child: RaisedButton(
                  color: Colors.blue,
                  textColor: Colors.white,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30),
                  ),
                  onPressed: _onScanButtonPressed,
                  child: Text(_isScannerEnabled ? 'Stop scanning' : 'Scan'),
                ),
              ),
            ),
          )
        ],
      )),
    );
  }
}
